// commentaire
/* message = "Message créé via JavaScript";
alert(message); */

// tableaux
users=["John","Olivia","Jack"];
message="Bienvenue dans le Sandbox JavaScript " + users[1] + ".";
console.log(message);
console.log("****");

// tableaux composés
account=[12345,"Olivia","Jones",true];
console.log("Employé N°"+account[0]+" ["+typeof(account[0])+"]");
console.log("Prénom : "+account[1]+" ["+typeof(account[1])+"]");
console.log("Nom : "+account[2]+" ["+typeof(account[2])+"]");
console.log("Statut : "+account[3]+" ["+typeof(account[3])+"]");
console.log("****");

//.length
console.log("Longueur du tableau : "+account.length);
console.log("****");

// fonctions
function showDryMessage() {
    console.log("Ne pas répéter.");
}
showDryMessage();
console.log("Une autre ligne.");
showDryMessage();
console.log("****");

// variables let
let season = "printemps";
console.log("Saison : "+season);
season="été";
console.log("Saison : "+season);
console.log("****");

// constantes
const pi=3.1416;
function getCircleArea(radius) {
    let area=pi*(radius**2);
    console.log("Aire : "+area);
}
getCircleArea(15);
console.log("****");

// portée locale et globale
function showFruit() {
    let fruit="citron";
    console.log("Fruit : "+fruit);
}
let fruit="pomme";
console.log("Fruit : "+fruit);
showFruit();
console.log("****");

// tableau via une fonction
function showCarData(car) {
    console.log("Marque : "+car[0]);
    console.log("Modèle : "+car[1]);
    console.log("Carburant : "+car[2]);
    console.log("Disponibilité : "+car[3]);
    console.log("Longueur du tableau : "+car.length);
}
let car=["Toyota","Yaris","Hybride",true];
showCarData(car);
console.log("*");
car=["Volkswagen","Polo","Essence",true];
showCarData(car);
console.log("****");

// fonction avec retour d'une valeur
const vatRate=21;
function getVatIncPrice(price) {
    let vatIncPrice=price+(price*vatRate/100);
    return vatIncPrice;
}
let vatIncPrice=getVatIncPrice(100);
console.log("Prix TTC : "+vatIncPrice);
function printStars() {
    console.log("****");
}
function printStars1() {
    console.log("*");
}
printStars();

// conditions if... else if... else...
if(2*3 == 6) {
    console.log("2 x 3 est bien égal à 6");
}
printStars1();
let active=true;
if(active) {
    console.log("La valeur de active est \"true\".");
}
printStars1();
let result=15;
if(result>100) {
    console.log("Le résultat est supérieur à 100.");
}
else{
    console.log("Le résultat est inférieur à 100.");
}
printStars1();
let stock=20;
if(stock>50) {
    console.log("Le résultat est supérieur à 50.");
}else if(stock > 5){
    console.log("Le résultat est entre 5 et 50.");
}else{
    console.log("Le résultat est inférieur ou égal à 5.");
}
printStars();

// opérateurs logiques && ||
let vegetable1="carotte";
let vegetable2="salade";
if((vegetable1=="carotte")&&(vegetable2=="salade")){
    console.log("Les 2 conditions sont respectées.");
}else{
    console.log("Les 2 conditions (ou une des 2) ne sont pas respectées.");
}
printStars1();
let fruit1="pomme";
let fruit2="citron";
if((fruit1=="pomme")||(fruit2=="citron")){
    console.log("Au moins 1 condition est respectée.");
}else{
    console.log("Aucune des conditions est respectée.");
}
printStars();

// instructions switch/case
let color="vert";
switch(color){
    case"rouge":
        console.log("Couleur choisie : rouge");
        break;
    case"vert":
        console.log("Couleur choisie : vert");
        break;
    case"bleu":
        console.log("Couleur choisie : bleu");
        break;
    default:
        console.log("Couleur choisie : aucun");
}
printStars1();
let item="oiseau";
switch(item){
    case"chat":
    case"chien":
    case"oiseau":
        console.log("Vous avez choisi un animal.");
        break;
    default:
        console.log("Choisissez un animal.");
}
printStars();

// boucles for... while... do/while...
let people=["James","Mike","Bill"];
for(let i=0;i<people.length;i++){
    console.log("Bienvenue "+people[i]);
}
printStars1();
function getRandomNumber(){
    let randomNumber=Math.floor(Math.random()*10);
    return randomNumber;
}
let randomNumber=getRandomNumber();
console.log(randomNumber);
printStars1();
while(randomNumber!==7){
    console.log(randomNumber+" n'est pas égal à 7");
    randomNumber=getRandomNumber();
}
console.log(randomNumber+" est bien égal à 7");
printStars1();
let counter=10;
while(counter<5){
    console.log(counter+" est plus petit que 5");
    counter++;
} // le code ne sera jamais exécuté
printStars1();
let count=1;
do{
    console.log(count);
    count++;
}while(count<1);
printStars();

// instruction continue
let saying="Qui restreint ses besoins sera d'autant plus libre.";
let vowels=["a","e","i","o","u","y"];
result=vowels.indexOf(saying.charAt(1));
console.log(result);
printStars1();
let vowelsCounter=0;
for(let i=0;i<saying.length;i++){
    let character=saying.charAt(i);
    if(vowels.indexOf(character)==-1){
        console.log(character+" n'est pas une voyelle.");
        continue;
    }
    vowelsCounter++;
}
printStars1();
console.log("Nombre de voyelles : "+vowelsCounter);
printStars();

// boucle infinie et instruction break
while(true){
    let randomNbr=getRandomNumber();
    if(randomNbr===8){
        break;
    }
    console.log(randomNbr+" n'est pas le chiffre mystère.");
}
console.log("Huit est le chiffre mystère.");
printStars();

// élément <a>
let links=document.getElementsByTagName("a");
console.log(links);
console.log(links[5]);
console.log(links.item(8));
printStars();

// modifier un élément via JS
let jsDOM1=document.getElementById("jsDOM1");
jsDOM1.innerHTML+=" / / Modifié via JavaScript";
let jsDOM2=document.getElementById("jsDOM2");
jsDOM2.style.color="green";
jsDOM2.style.fontSize="130%";
jsDOM2.style.fontWeight="bold";
let jsDOM3=document.getElementById("jsDOM3");
jsDOM3.setAttribute("class","jsDOM3");

// créer un élément via JS
const jsDOM4=document.createElement("p");
// const content4=document.createTextNode("Contenu temporaire");
// jsDOM4.appendChild(content4);
const main=document.getElementById("main");
main.appendChild(jsDOM4);
jsDOM4.setAttribute("class","jsDOM4");
jsDOM4.innerHTML="Créé via JavaScript";

const jsDOM5=document.createElement("button");
main.appendChild(jsDOM5);
jsDOM5.setAttribute("class","jsDOM5");
jsDOM5.innerHTML="cacher";
jsDOM5.onclick=function() {hideParagraph("jsDOM3")};
function hideParagraph(id){
    let para=document.getElementById(id);
    if(para.style.display==="none"){
        para.style.display="block";
        jsDOM5.innerHTML="cacher";
    }else{
        para.style.display="none";
        jsDOM5.innerHTML="afficher";
    }
}

// Créer une balise <img> pour un diaporama automatique
const sliderImage=document.createElement("img");
const sliderMessage=document.createElement("p");
const list=document.getElementById("main");
console.log(list.childNodes);
list.insertBefore(sliderImage,list.childNodes[3]);
sliderImage.setAttribute("src","img/slider1.jpg");
sliderImage.setAttribute("class","slider_image");
sliderImage.setAttribute("id","slider_image");
sliderImage.onclick =function(){toggleSlider()};
list.insertBefore(sliderMessage,list.childNodes[4]);


// Fonction de gestion des images du diaporama
function showSlider(){
  image=document.getElementById("slider_image");
  imageName="img/slider"+imageIndex + ".jpg";
  image.setAttribute("src",imageName);
  if(imageIndex<numberOfImages){
    imageIndex++;
  }else{
    imageIndex=1;
  }
}

// Fonction "marche/arrêt" du diaporama
function toggleSlider(){
  if(slider===true){
    clearInterval(interval);
    sliderMessage.setAttribute("class","message message_start");
    sliderMessage.innerHTML = "Cliquez sur l'image pour lancer le diaporama";
    slider=false;
  }else{
    showSlider();
    interval = setInterval(showSlider,timer);
    sliderMessage.setAttribute("class","message message_stop");
    sliderMessage.innerHTML="Cliquez sur l'image pour arrêter le diaporama";
    slider=true;
  }
}

// Initialisation du diaporama automatique
imageIndex=1;
numberOfImages=3;
timer=3000; //millisecondes
slider=false;
toggleSlider();